#include <stdio.h>
#include <stdlib.h>
#include <time.h>

//Se busca encontrar a una celebridad, para ello en la la tabla de la poblacion donde hay una,
//todas las perzonas de (las pociciones de i) la arrojan una coincidencia en la pocicion (j=x)
//pero la pcicion (i = x) unicamente reconoce a la pocicion (j = x) que es si misma y segun el 
//enunciado : "Todas las demás personas de la población conocen a c, y c no conoce a ninguna 
//persona de la población, con la excepción de a sı́ misma." 


//imprie 0 en toda la tabla exepto cuando es la misma persona
void imprimirtabla(int tabla[6][6]){
	
		printf("su tabla es quedaria:\n");
		
		for (int i = 0; i < 6; i ++){ //realiza el recorrido para asignar valores a las cordenadas 
			for (int j = 0; j< 6; j ++){
			
			tabla[i][j] = 0;
			
			if (i == j){
				tabla[i][j] = 1;
			}
			
			printf(" %d",tabla[i][j]);
		}
		printf("\n");
	}
}

//imprime 1 aleatoreamente cuando se "conocen"
void Conocidos(int tabla[6][6]){
	

		for (int i = 0; i < 6; i ++){ 
			for (int j = 0; j< 6; j ++){
			
			i= (rand()%6);
			j= (rand()%6);
			
			tabla[i][j] = 1;
			
			printf(" %d",tabla[i][j]);
		}
		printf("\n");
	}
}
//funcion principal donde se agregan variables y se deriban a las funciones 
int main()
{
	
	srand(time(0));
	int tabla[6][6];
	
	imprimirtabla(tabla);
	Conocidos(tabla);
	return 0;
}


