#include <stdio.h>

//~ Escriba un programa que, dada una frase compuesta solamente por
//~ letras minúsculas, imprima la misma frase pero dejando la primera letra de cada palabra
//~ en mayúscula.

int main()
{
	char alfabeto[36] = {"abcdefghijklmnopqrstuvwxyzáéíóú"};
	char mayuscula[36] = {"ABCDEFGHIJKLMNOPQRSTUVWXYZÁÉÍÓÚ"};
	char extra[100]={";:.,ü{}[]^-'ñ<>¡!¿?"};//signos de puntuacion y otras cosas que no cambiarian
	char ingreso[100];
	char titulo[100];
	
	
	printf("ingrese el titulo deseado\n");
	scanf("%[^\n]s", ingreso);
	
	for (int i=0; i<36; i++){//primero se revisaran los signos de puntuacion etre otros 
		for(int j=0; j<100; j++){
			
			if (ingreso[i] == extra[j]){
				titulo[i] = extra[j];
				
			}
		}
	}
	
	for (int i = 0 ; i < 36; i++){//se recorrera el arrreglo inicial 
		for (int j = 0 ; j < 36; j++){
			
			//convertir la frase entregada 
			if (ingreso[i] == ' '){
				titulo[i] = ' ';
				//cuando se detecte i como un espacio que i+1 sea mayuscula
				if (ingreso[i+1] == alfabeto[j]){
					titulo[i+1] = mayuscula[j];
				}
				
				if (ingreso[i] == alfabeto[j]){
					titulo[i] = alfabeto[j];
				}
			}

		}
	}
	printf("%s\n", titulo);
	
	
	
	return 0;
}

