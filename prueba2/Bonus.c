
acertijo numero 1

		>enunciado:
	Un hombre murió y fue al Paraı́so. Habı́a miles de personas, todas desnudas y con la apariencia
	que tenı́an a los 21 años. Miró alrededor tratando de reconocer a alguien. Súbitamente vio a
	una pareja y supo que se trataban de Adán y Eva
	
		>pregunta:
	¿Cómo lo supo?
	
		>respuesta:
	Adán y Eva eran una pareja que habia conocido mientras estaba vivo

acertijo numero 2 

		>En en el acertijo se dice:
	Se sabe que los ((caballeros)) siempre decı́an la verdad mientras que los ((escuderos)) siempre mintieron
	
		>respuesta:
	por lo tanto depede del rango que posea C ya que en el caso de que sea ((caballero)), si dice que B esta mintiendo,
	significa que B seria ((escudero))
	pero en el caso de que C sea ((escudero)) entonces miente sobre el hecho de que B esta mintiendo, lo que haria que 
	B diga la verdad y  los ((caballeros)) siempre dicen la verdad 
	
		>respuesta opcional:
	Son habitantes de Daniel Defoe (en caso de estar bien esta respuesta coinciderela como valida por favor)
	

acertijo numero 4

		>pregunta: 
	¿De quién es esa foto grafı́a?
	
		declaracion:
	Ni hermanos ni hermanas tengo, pero el padre de este hombre es el hijo de mi padre
	
		>respuesta:
	la persona del retrato es la misma a la que se le hace la pregunta
	
acertijo numero 6
	
	enunciado:
	Un tren sale de Talca para Santiago. Una hora después otro tren sale de Santiago para Talca.
	Los dos trenes van exactamente a la misma velocidad. 
	
	>pregunta:
	¿Cuál de los dos estará más cerca de Talca cuando se encuentren?
	
	>respuesta:
	se encuentran a la misma distania ya que se encontraron en el mismo punto 
	
