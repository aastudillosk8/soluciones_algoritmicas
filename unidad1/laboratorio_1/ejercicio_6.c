#include <stdio.h>
#include <stdlib.h>
//~ Una persona se encuentra en el kilómetro 190 de la carretera 5 sur entre Curicó y Talca, otra se
//~ encuentra en el km 250 de la misma carretera, la primera viaja en dirección a Talca, mientras que la
//~ segunda se dirige a Curicó, ambos en la misma velocidad. Realice un algoritmo para determinar en qué
//~ kilometro de esa carretera se encontrarán y represéntelo en un programa C y un algoritmo narrado.

int main(void)
{
	int persona1;
	int persona2;
	int km_a_recorrer;
	int distancia;
	int punto_encuentro;
	//comenzar a narrar 
	printf("iniciar viaje  \n" );
	printf("### tiene un mensaje de 'waze' : ubicacion actual Km 190, una patrulla se direje a usted desde el kilometro 250. ### \n" );
	printf( "patrulla 'Km 250' ----------------------------------------------------- usted 'km 190' \n");
	//calcular punto de encuentro
	persona1=190;
	persona2=250;
	distancia = persona2 - persona1;
	km_a_recorrer = distancia / 2;
	punto_encuentro = persona1 + km_a_recorrer;
	

	printf("distancia restante: %d\n" ,km_a_recorrer);
	printf( " patrulla 'Km 240' -------------------------------- usted 'km 200', distancia restante 20Km \n");
	printf( "       patrulla 'Km 230' --------------------- usted 'km 210', distancia restante 10Km \n");
	printf( "             patrulla 'Km 230' --------- usted 'km 210', distancia restante 5Km \n");	
	printf( "                patrulla 'Km 230' -|- usted 'km 210', patrulla en frente \n");
	printf("se ha encontrado con la patrulla en el Km: %d\n" , punto_encuentro);
	printf("gracias por prefeir Waze ;D \n");
	printf("hasta pronto\n");
	
	return 0;
}

