#include <stdio.h>
#include <stdlib.h>

void elecciones(char candidato1[10], char candidato2[10], char candidato3[10]){
	char x;
	int voto_actual, eleccion1, eleccion2, eleccion3, nulos, total;
	eleccion1 = 0;
	eleccion2 = 0;
	eleccion3 = 0;
	nulos = 0;
	total = 0;
	
	//comensar ciclo
	while (x != 'F' && x!= 'f'){
		printf ("Ingrese voto:\n");
        scanf ("%d", &voto_actual);
        
        //añadir votos
		switch(voto_actual){
			case 1:
				eleccion1++;
				total++;
				break;
            
			case 2:
				eleccion2++;
				total++;
				break;
            
			case 3:
				eleccion3++;
				total++;	
				break;
        
			default: 
				nulos++;
				total++;
				break;
		
		}

		voto_actual = 0;

		
		printf ("si desea terminar ingrese f o F o ingrese otra letra para continuar:\n ");
		scanf (" %c",&x);
		
	}
	
	//total = eleccion1 + eleccion2 + eleccion3 + nulos;
	
	// porcentajes :
	float porcentaje_nulos=0;
	porcentaje_nulos = (nulos * 100) / total;
	float porcentaje_eleccion1=0;
	porcentaje_eleccion1 = (eleccion1 * 100) / total;
	float porcentaje_eleccion2=0;
	porcentaje_eleccion2 = (eleccion2 * 100) / total;
	float porcentaje_eleccion3=0;
	porcentaje_eleccion3 = (eleccion3 * 100) / total;
	//comentar votos para cada persona 
	printf ("total de votos: %d\n",total);
	printf ("Total de nulos:%d, con un porcentaje de %.02f\n",nulos, porcentaje_nulos);	
	printf ("Los votos obtenidos por %s son: %d, con un porcentaje de %.2f\n",candidato1,eleccion1,porcentaje_eleccion1);
	printf ("Los votos obtenidos por %s son: %d, con un porcentaje de %.2f\n",candidato2,eleccion2,porcentaje_eleccion2);
	printf ("los votos obtenidos por %s son: %d, con un porcentaje de %.2f\n",candidato3,eleccion3,porcentaje_eleccion3);   

   
   
   //declarar ganador 
    if ((eleccion2<eleccion1)&&(eleccion1>eleccion3)){
        printf ("¡Felicidades!  Ha ganado %s : ", candidato1);	
    }
    if ((eleccion1<eleccion2)&&(eleccion2>eleccion3)){
	   printf (" ¡Congratulations!, Ha ganado %s: ", candidato2);
	}
    if ((eleccion3>eleccion2)&&(eleccion3>eleccion1)){
	   printf ("¡congratulations! , Ha ganado %s : ", candidato3);
	}
	
	else if (eleccion1 == eleccion2){
		printf("se ha realizado un empate entre 1 y 2, realice nuevamente las votaciones"); 
	}
	else if (eleccion2 == eleccion3){
		printf("se ha producido un empate entre 2 y 3, realice nuevamente las votaciones");
	}
}

//ingresar candidatos preferido
void ingreso_candidatos(char candidato1[10], char candidato2[10], char candidato3[10]){
	printf ("ingrese candidato 1:\n");
	scanf ("%s", candidato1);
	printf ("ingrese candidato 2:\n");
	scanf ("%s", candidato2);
	printf ("ingrese candidato 3:\n");
	scanf ("%s", candidato3);
	system("clear");
	
}

//
int main(void){
	char candidato1[10];
	char candidato2[10];
	char candidato3[10];
	ingreso_candidatos(candidato1,candidato2,candidato3);
	for ( int i=0; i<3; i++){
		printf ("##########################################\n");
		printf ("######################################## PREPARANDO ELECCIONES\n########################################\n");
	}
	printf ("------------------------------------------------------------\n");
	elecciones(candidato1,candidato2,candidato3);

	return 0;
}

