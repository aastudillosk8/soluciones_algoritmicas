
#include <stdio.h>
#include <stdlib.h>
//~ La asociación de vinicultores tiene como polı́tica fijar un precio inicial al kilo de uva, la cual se clasifica
//~ en tipos A y B, y además en tamaños 1 y 2. Cuando se realiza la venta del producto, ésta es de un
//~ solo tipo y tamaño, se requiere determinar cuánto recibirá un productor por la uva que entrega en
//~ un embarque, considerando lo siguiente: si es de tipo A, se le cargan $20 al precio inicial cuando es
//~ de tamaño 1; y $30 si es de tamaño 2. Si es de tipo B, se rebajan $30 cuando es de tamaño 1, y $50
//~ cuando es de tamaño 2.

//diferenciar uvas por tipo y tamaño
void precio_final(float kilo_uva, char tipo_uva, int tamano_uva){
	if (tipo_uva == 'A' && tamano_uva == 1){
		kilo_uva = kilo_uva + 20;
	}
	if (tipo_uva == 'A' && tamano_uva == 2){
		kilo_uva = kilo_uva + 30;
	}
	if (tipo_uva == 'B'  && tamano_uva == 1){
		kilo_uva = kilo_uva - 30;
	}
	if (tipo_uva == 'B'  && tamano_uva == 2){
		kilo_uva = kilo_uva - 50;
	}
	printf ("El precio de la uva es : %.02f",kilo_uva);
}

void compra(){
	float kilo_uva;
	char tipo_uva, compra;
	int tamano_uva;
	//calcular valor y precio para el cliente 
	printf ("Bienvenido:\n");
	printf ("listo para comprar? ingrese 's':\n");
	scanf ("%c",&compra);
	if (compra == 's'){
		printf ("ingrese precio inicial al kilo de uvas:\n");
		scanf ("%f",&kilo_uva);
		printf ("Seleccione el tipo de uva: tipo 'A' o 'B'\n");
		scanf (" %c",&tipo_uva);
		printf ("ingrese tamano de uva: tipo '1' o '2'\n");
		scanf ("%d",&tamano_uva);
		precio_final(kilo_uva,tipo_uva,tamano_uva);
	}			
	
}

int main(void){
	compra();
	return 0;
}

