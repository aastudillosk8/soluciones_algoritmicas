#include <stdio.h>

//~ Pepe salió a celebrar su santo con un grupo de amigos y decidieron ir a comer a un restaurante. Ellos
//~ acostumbran pagar la cuenta por partes iguales, es decir, cada uno paga lo mismo. Sin embargo, la
//~ cuenta incluye el consumo de todos los comensales, y no considera el IVA (19 %) ni la propina. Pepe
//~ y su grupo de amigos acostumbran dejar un 10 % como propina. El problema es que no saben cuánto
//~ deben pagar cada uno. Construya un programa en C que lea la cantidad de amigos con que salió Pepe
//~ y el total de lo consumido (sin IVA). El programa debe indicar cuanto debe pagar cada uno, incluyendo
//~ el IVA y la propina, que en este caso se aplica al valor de la cuenta más IVA.

int main(int argc, char **argv)
{
	float total;
	float iva;
	float propina;
	float cadauno;
	int amigos;
	float totalfin;
	// se le pregunta al usuario cuanto costo la comida
	printf ("¿cuanto costo la comida?\n");
	scanf ("%f",&total);
	
	//se calcula el iva
	iva= (19*total)/100;
	
	// se calcula la propina
	propina= (10*total)/100;
	
	// se calcula el total final
	totalfin= (iva+propina+total);
	
	
	printf ("el total del consumo incluyendo el iva y la propina es :%.2f\n", totalfin);
	
	//se consulta la cantidad de amigos que fueron a comer 
	printf ("ingrese la cantidad e amigos: \n");
	scanf ("%d",&amigos);
	
	//se subdivide el total de la cuenta entre todos los amigos por igual
	cadauno=(totalfin/amigos);
	printf ("cada uno debe pagar :%.2f",cadauno);

	return 0;
}

