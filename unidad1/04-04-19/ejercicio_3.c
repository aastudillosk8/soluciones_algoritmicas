#include <stdio.h>

//~ La tienda “El harapiento distinguido” tiene una promoción: a todos los trajes que tienen un precio
//~ superior a $2500.00 se les aplicará un descuento de 15 %, a todos los demás se les aplicará sólo 8 %.
//~ Realice un algoritmo para determinar el precio final que debe pagar una persona por comprar un traje
//~ y de cuánto es el descuento que obtendrá.

int main(void)
{
	float valortraje;
	float descuento15;
	float cantidad_descuento;
	float descuento8;
	float precio_final;
	printf ("ingrese el precio del traje\n ");
	scanf ("%f\n",&valortraje);
	
	//Si el valor es mayor a 250000 se le efectuara el descuento de temporada de 15%
		if (valortraje>=2500.00){
			printf ("¡¡¡¡¡¡¡Su traje tiene un descuento de temporada!!!!\n");
			//se calcual el valor que tendra el traje 
			descuento15= (0.85*valortraje);
			//se calcula el descuento obtenido
			cantidad_descuento = (descuento15 - valortraje);
			printf ("Su traje tiene un valor con descuento de : %f \n",descuento15);
			printf ("el descuento aplicado a su traje fue de: %f \n" ,cantidad_descuento);
}
// Si el valor es menor a 250000 se le efectuara el descuento de temporada de 8%
		else if (valortraje<=2500.00){
			printf ("su traje tiene un hermoso descuento de temporada otoño - invierno");
			//se calcula el descuento que se obtendra 
			descuento8=(0.08*valortraje);
			//se aplica el descuento al traje a comprar
			precio_final= valortraje - descuento8;				
			printf("su hermoso traje vale: %f.2\n" ,precio_final);
			printf ("El descuento del traje es: %f.2\n",descuento8);
}

//se finaliza programa//
	return 0;
}

