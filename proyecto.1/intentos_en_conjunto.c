#include <stdio.h>
#include <stdlib.h>
#include <time.h>

//imprime las letras de la matriz aleatorea
void llenar_letras_genes(int N, char gen[N][N])
{
	int valor;
	
	for (int i = 0; i< N; i++){
		for (int j = 0; j < N; j++){
			
			valor = (rand()%4)+1;
			
			switch (valor){
				
				case 1:
					gen[i][j] = 'c';
					break;
					
				case 2:
					gen[i][j] = 'g';
					break;
					
				case 3:
					gen[i][j] = 'a';
					break;
					
				case 4:
					gen[i][j] = 't';
					break;					
				}	
			
				printf("|%c|", gen[i][j]);
			}
			printf("\n");
		}
	}

//~ //cuenta la pocicion de cada valor (sin utilidad de momento)
//~ void contador(int N, char gen[N][N])
//~ {	
	//~ int i, j;
	//~ //int mutacion_tipo_A = 0;
	
	//~ for (i = 0; i< N; i++){
		//~ for (j = 0; j < N; j++){
			//~ if (gen[i][j] == 'a'){
				//~ printf("se ha encontrado 'A' en la posicion %d - %d\n",i ,j);
				//~ //mutacion_tipo_A++;
			//~ }
		//~ }
	//~ }
	//~ //printf("mutacion = %d", mutacion_tipo_A);
//~ }

//comprobar condiciones de las mutaciones para generar una nueva matriz
char construir_gen(int N, char gen[N][N]){
	
	int mutacion;
	int i, j;
	
	for (i = 0; i< N; i++){
		for (j = 0; j < N; j++){
			//printf("\n %c \n" ,gen[i][j]);
			
						
							for (int x = i-1; x <= i+1; x++){
								for (int z = j-1; z <= j+1; z++){
									//intento de condicionales para no evaluar fuera de la matriz
									//~ if (i != 0){
										//~ if (j != 0){
											//~ if (i != N-1){
												//~ if(j != N-1){
										
										//intento de comparar condiciones de cada mutacion
									if (gen[i][j] == 'a'){
										mutacion = 0;
						
										if (gen[x][z] == 'g'){
												
											mutacion++;
											if (mutacion >= 3){
												
												return 'T';
											}	
										}
									}
									
									if (gen[i][j] == 't'){
										mutacion = 0;
						
										if (gen[x][z] == 'c'){
												
											mutacion++;
											if (mutacion >= 3){
												
												return 'a';
											}
										}		
											
									}
									if (gen[i][j] == 'g'){
										mutacion = 0;
						
										if (gen[x][z] == 'a'){
											
											mutacion++;
											if (mutacion >= 3){
												
												return 'c';
											}
										}
									}
									
									if (gen[i][j] == 'c'){
										mutacion = 0;
						
										if (gen[x][z] == 't'){
											
											mutacion++;
											if (mutacion >= 3){
												
												return 'g';
											}
										}
									}																													
								//printf("\n se encontraron %d g", mutacion);
								
								
														
								}
							}
						}
					}
					
				}
			
			//~ }
		//~ }
	//~ }
//~ }

void imprimir(int N, char gen_temp[N][N]){
	
	for (int i = 0; i< N; i++){
		for (int j = 0; j < N; j++){	
	
			printf("|%c|", gen_temp[i][j]);
		}
		printf("\n");
	}
}


//funcion principal deonde derivo al resto de las funciones 
int main()
{	
	int N;
	srand(time(0));
	printf("ingrese el largo del genoma: ");
	scanf("%d", &N);
	
	char gen[N][N];
	char gen_temp[N][N];

	llenar_letras_genes(N, gen);
	contador(N, gen);
	gen_temp[N][N] = construir_gen(N, gen);

	imprimir(N, gen_temp);
	return 0;

}
