#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

	//imprime las letras de la matriz aleatorea
void llenar_letras_genes(int N, char gen[N][N])
{
	int valor;
	
	for(int i=0; i < N*3 + 2; i++){
		printf("=");
	}
	printf("\n");
	
	for (int i = 0; i< N; i++){
		printf("=");
		
		for (int j = 0; j < N; j++){
			
			valor = (rand()%4)+1;
			
			switch (valor){//a cada valor le asigno una letra
				
				case 1:
					gen[i][j] = 'c';
					break;
					
				case 2:
					gen[i][j] = 'g';
					break;
					
				case 3:
					gen[i][j] = 'a';
					break;
					
				case 4:
					gen[i][j] = 't';
					break;					
			}	
			
			printf("|%c|", gen[i][j]);
		}
		printf("=");
		printf("\n");
	}
	
}


void contador(int N, char gen[N][N])
{	
	int i, j;
	int mutacion_tipo_A = 0;
	
	for (i = 0; i< N; i++){
		for (j = 0; j < N; j++){
			if (gen[i][j] == 'a'){
				printf("se ha encontrado 'A' en la posicion %d - %d\n",i ,j);
				mutacion_tipo_A++;
			}
		}
	}
	printf("mutacion = %d", mutacion_tipo_A);
}

//comprobar condiciones de las mutaciones para generar una nueva matriz
void construir_gen(int N, char gen[N][N]){
	
	int mutacion;
	int i, j;
	
	for (i = 0; i< N; i++){
		for (j = 0; j < N; j++){
			
			if (gen[i][j] == 'A'){
				mutacion = 0;
					for (int x = i-1; x <= i+1; x++){
						for (int z = j-1; z <= j+1; z++){
			
							if (gen[x][z] == 'G'){
								
								mutacion++;
								
							}
							
						}
					}
					printf("\n se encontraron %d g", mutacion);

			}
			
		}
	
	}


}
//funcion en donde derivo a todas las funciones 
int main()
{
	printf(" hola\n");
	printf(" binevenido al menu de inicio \n");

	int N;
	srand(time(0));
	printf("ingrese el largo del genoma: ");
	scanf("%d", &N);
	
	char gen[N][N];

	llenar_letras_genes(N, gen);
	contador(N, gen);
	construir_gen(N, gen);
	
	return 0;
}

