#include <stdio.h>
#include <stdlib.h>

// funcion que imprime valores de la matriz
void imprimirMatriz(int M, int N, int a[M][N]){
		for (int i = 0; i < M; i ++){ 
			for (int j = 0; j< N; j ++){
			printf(" %d",a[i][j]);
		}
		printf("\n");
	}
}

// asignar valores a la matrir e enviarlos a la funcion imprimir_matriz
void CrearMatriz(int M, int N,int a[M][N]){
	for (int i = 0; i < M; i++){
		for(int k = 0; k < N; k++){
			
			printf ("(%d,%d)",i,k);
			scanf (" %d",&a[i][k]);
		
		}
	}
		imprimirMatriz(M,N,a);

}

//invierte los valores de las filas  en la matriz
void MatrizInversa(int M, int N,int a[M][N]){
	printf ("Matriz simetrica\n");
	int i= M -1;
	
	for ( M=-1; i>-1; i--){
		for (int j=0; j<N; j++){
			printf(" %d",a[i][j]);
		}
		printf ("\n");
	}
}

//funcion principal donde se agregan valores a las variables y se envian a las funciones
int main()
{
	int M;
	int N;
	
	printf("ingrese un valor para M");
	scanf("%d", &M);
	
	printf("ingrese un valor para N");
	scanf("%d", &N);
	
	int a[M][N];
	CrearMatriz(M,N, a);
	MatrizInversa(M,N,a);
	return 0;
}
