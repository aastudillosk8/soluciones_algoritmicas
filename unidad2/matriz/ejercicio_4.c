#include <stdio.h>

//~ En una estacion meteorologica registramos la temperatura (en grados centigrados) cada hora durante
//~ una semana. Almacenamos el resultado en una matriz de 7 X 24 (cada fila de la matriz contiene las 24
//~ mediciones de un dia). Diseña un programa que lea los datos por teclado y muestre:
//~ La maxima y minima temperaturas de la semana.
//~ La maxima y minima temperaturas de cada dia.
//~ La temperatura media de la semana.
//~ La temperatura media de cada dia.
//~ El numero de dias en los que la temperatura media fue superior a 30 grados.

int main (){
	int N;
	
	printf("Ingrese el valor para N:\n ");
	scanf("%d", &N); 
	
	int matriz[N][N]; 
	int i, j;
	int simetria_matriz=0,valor; 
	
	//el usuario ingresa los valores de las coordenadas 
	for(i=0; i<N; i++){
		for(j=0; j<N; j++){
				
				printf("Rellene la matriz[%d][%d]:\n", i, j); 
				scanf("%d", &valor);
				
				matriz[i][j] = valor;
		}
	}
		
	//Se comparan los indices para comprobar la simetria 
	for(i=0; i<N; i++){
		for(j=0; j<N; j++){
			
			if(j!=i){ //se ingresa la condicion 
				
				if(matriz[i][j] == matriz[j][i]){ //segunda condicion que de la posicion i j es igual a la posicion j i
					
					simetria_matriz++; //el contador aumentara en 1 cada vez que se cumple la condicion
				}
			}
		}
	}	
	
	for(i=0; i<N; i++){
		for(j=0; j<N; j++){
			
			printf(" %d", matriz[i][j]); //Se imprimira la matriz cuadrada 
		
		}
		
		printf("\n"); 
	}
	
	if(N == 1){ 
		
		printf(" no se encuenta simetria");
	}
	
	else{
		
		int es_simetrico = (N*N)-N;
		
		if(simetria_matriz == es_simetrico){//condicion de igualdad
			
			printf("La matriz es simetrica");
		}
		else{ 
			
			printf("La matriz no es simetrica");
		}
	}
	
	return 0;
}

