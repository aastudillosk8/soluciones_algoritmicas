//~ Dada una matriz A (MxN) de tipo entero, construya un programa para calcular la Traspuesta de dicha
//~ matriz. La traspuesta de una matriz se obtiene al escribir las filas de la matriz A como columnas. 
//~ 1 <= M <= 50 y 1 <= N <= 30

#include <stdio.h>
#include <time.h>
#include <stdlib.h>

int main()
{
	int M, N, i, j;
	
	srand(time(0));//se genera numeros random para rellenar la matriz
	
	printf("Ingrese dos numeros para determinar el N y el M de una matriz\n");
	scanf("%d",&M);
	scanf("%d",&N);
	
	if(1>M || M>50 || 1>N || N>30){//condiciones de los parametros para la matriz
	printf("los valores no son correctos\n");
	
	return main();
	}
	else {
		

	int matriz_ing[N][M];
	
	
	for (i=0; i<N; i++){//se rellena la matriz cn los numeros random
		for (j=0; j<M; j++){
		matriz_ing[i][j]=rand()%100+1;
		
    }
  }
  
  printf("\n\n Matriz original \n\n");
  
  for (i=0; i<N; i++){//se imprime la matriz original
    for (j=0; j<M; j++){
		printf(" %d", matriz_ing[i][j]);
	}
    printf(" \n");
  }
	
	printf("\n Matriz transpuesta\n");
	
	//imprime matriz nueva
	for (i=0; i<M; i++){
		for (j=0; j<N; j++){
		printf(" %d", matriz_ing[j][i]);
	}
    printf(" \n");
  }
}
	return 0;
}

