#include <stdio.h>

//~ Cree una matriz cuadrada A de NxN, luego pida al usuario rellenarla. Al final del proceso, entregue al
//~ un resumen con: La matriz, la sumatoria de los valores de sus filas y luego la sumatoria de los valores
//~ de toda la matriz.

int main()
{
	int M;
	
	printf("agrege el largo para NxN de la matriz");
	scanf("%d", &M);
	
	int a[M][M];
	int i,j;
	int sumatoria=0;
	
	for (i=0;i<M;i++){//ingresar valores para las coordenadas 
		for (j=0;j<M;j++){
		
			printf ("Ingrese valores para la coordenada (%d,%d):\n",i,j);
			scanf ("%d",&a[i][j]);
		}
	}
	
	//imprimir la coordenadas 
	for ( i=0;i<M;i++){
		for ( j=0;j<M;j++){
		
			printf ("%d ",a[i][j]);		
		}
		printf ("\n");
	}
	
	//realizar sumatorias
	for ( i=0;i<M;i++){
		for ( j=0;j<M;j++){
			
			sumatoria=sumatoria + a[i][j];
		
		}
		printf ("fila %d: %d",i+1, sumatoria);
		printf ("\n");
		
		sumatoria=0;
	}
	
		for (i=0;i<M;i++){
			for ( j=0;j<M;j++){
		
				sumatoria=sumatoria + a[i][j];
		
			}
		printf ("\n");
	}
		printf ("La matriz suma: %d", sumatoria);	
		
	return 0;
}

