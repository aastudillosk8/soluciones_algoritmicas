#include <stdio.h>
#include <stdlib.h>
#include <time.h>

//~ Escriba un programa que cree una matriz cuadrada A de NxN, luego rellénela de ceros y asigne 1 en
//~ su diagonal principal 1 . En otras palabras genere una matriz identidad 2 de 6x6.

int main()
{
	int M;
	
	srand(time(0));//ingresar la semilla para valores aleatoreos
		
	M = (rand()%10);//darle un valor aleatoreo a M
	
	int a[M][M];
	int i, j;

	printf(" M es igual a :%d\n", M); //imprimir el numero para comprobar
	
	//asignar valores 0 a las coredenadas 
	for (i = 0; i<M; i++){ 
		for (j = 0; j<M; j++){
			
			a[i][j] = 0;
			
			//la diagonal con valor 1
			if (i == j){
				printf("1 ");
			}
			else {
				printf("%d ", a[i][j]);
			}
			
		}
		printf("\n");
	}
	return 0;
}

