#include <stdio.h>
#include <stdlib.h>
#include <time.h>

//~ Escriba un programa que cree una matriz cuadrada A de NxN, luego rellénela de ceros e imprı́mala en
//~ pantalla.

//funcion para que se imprima la matriz cuadrada 
void imprimirMatriz(int N, int a[N][N]){
	
		printf("N es: %d\n", N);
		
		for (int i = 0; i < N; i ++){ //realiza el recorrido para asignar valores a las cordenadas 
			for (int j = 0; j< N; j ++){
			
			a[i][j] = 0;
			
			printf(" %d",a[i][j]);
		}
		printf("\n");
	}
}

//funcion principal donde se agregan variables y se deriban a las funciones 
int main()
{

	int N;
	
	srand(time(0));
	N = (rand()%10)+3;
	
	int a[N][N];
	
	imprimirMatriz(N, a);
	return 0;
}
