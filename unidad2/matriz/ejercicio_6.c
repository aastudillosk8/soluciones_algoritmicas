#include <stdio.h>
#include <stdlib.h>
#include <time.h>

//~ En una estación meteorológica registramos la temperatura (en grados centı́grados) cada hora durante
//~ una semana. Almacenamos el resultado en una matriz de 7 X 24 (cada fila de la matriz contiene las 24
//~ mediciones de un dı́a).

//se rellena las matrices con valores aleatoreos
void llenar_matriz(int dias, int horas, int matriz[dias][horas] ){
	
	srand(time(NULL));
	int num;
	
	for(int i=0; i<dias;i++){
		for (int j=0; j<horas; j++){
	
			num =(rand()%42)+1; 
			matriz[i][j]=num;
			
		}
	}

}

//declara cuales son las menores y mayores temperaturas de cada dia
void grados_dia(int dias, int horas, int matriz[dias][horas]){
	int mayor, menor;
	
	for(int i=0; i<dias;i++){
		mayor=0;
		menor=100;
		for (int j=0; j<horas; j++){
			if (menor< matriz[i][j]){
			}
			else{
				menor= matriz[i][j];
			}
			if (mayor> matriz[i][j]){
			}
			else{
				mayor= matriz[i][j];
			}
		}
		printf("la T° maxima de dia %d es:  [%d°C]\n", i+1, mayor);
		printf("la T° minima de dia %d es:  [%d°C]\n", i+1, menor);
	} 
	printf("\n");
	
}

//determina la menor y la mayor temperatura de la semana 
void grados_semana(int dias, int horas, int matriz[dias][horas]){
	printf ("\n");
	int mayor=0, menor=100;
	
	for(int i=0; i<dias;i++){
		for (int j=0; j<horas; j++){
			if (menor< matriz[i][j]){
			}
			else{
				menor= matriz[i][j];
			}
			if (mayor> matriz[i][j]){
			}
			else{
				mayor= matriz[i][j];
			}
		}
	} 
	printf("\n");
	printf("T° maxima de la semana es:  [%d°C]\n", mayor);
	printf("T° minima de la semana es:  [%d°C]\n", menor);
}

//funcion que imprime la matriz 
void imprimir_matriz(int dias, int horas, int matriz[dias][horas]){
	for(int i=0; i<dias;i++){
		printf ("Dia %d:",i+1);
		for (int j=0; j<horas; j++){
			printf("[%d] ", matriz[i][j]);
		}
		printf("\n");
	}
	
}

//funcion que calcula el promedio de temperatura de la semana 
void media_semana_dia(int dias, int horas, int matriz[dias][horas]){
	int suma=0 ;
	
	printf("\n");
	
	for(int i=0; i<dias;i++){
		for (int j=0; j<horas; j++){
			suma= (suma + matriz[i][j]);
		}
		suma = suma/24;
		printf("La media del dia %d es: [%d°C]\n",i+1,suma);
		suma=0;
	}
	
}

//saca cuales fueron los dias que superaron los 30 °C
void media_superior(int dias, int horas, int matriz[dias][horas]){
	int suma=0, contador_dias=0, media=0 ;
	
	printf("\n");
	
	for(int i=0; i<dias;i++){
		for (int j=0; j<horas; j++){
			suma= (suma + matriz[i][j]);
			
		}
		media= suma/24;
		
		if (media>30){
			contador_dias=contador_dias+1;
		}
		suma=0;
	}
	printf("Dias con la media superior a 30 °C: [%d]\n", contador_dias);
	
}

//funcion principal que deriba a los void 
int main(){
	printf ("-------------------------------Programa de medición de T°---------------------------------------\n");
	int dias = 7;
	int horas = 24;
	int matriz[dias][horas];
	
	llenar_matriz(dias, horas, matriz);
	
	imprimir_matriz(dias, horas, matriz);
	
	printf ("\n");
	
	grados_dia(dias, horas, matriz);
	
	grados_semana(dias, horas, matriz);
	
	media_semana_dia(dias, horas, matriz);
	
	media_superior(dias, horas, matriz);
	

	return 0;
}
