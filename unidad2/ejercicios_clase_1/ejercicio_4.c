#include <stdio.h>
#include <string.h>

int main(int argc, char **argv)
{
	char frase[100];    //se ingrese una cantidad de terminada de caracteres como maximo 
	char caracter;      //variable para guardar el caracter a buscar
	int contar;         // se guardan la cantidad de vese que se repite el caracter 
	int largo;          // una variable para guardar el fargo para utilizar el for 
	
	printf("inrgrese una frase para comenzar\n");
	gets(frase);        //se ingresa una frase determinada por el usuario 
	
	printf("ingrese su caracter a buscar\n");
	scanf("%c", &caracter);//se ingresa el caracter a buscar
	
	largo = strlen(frase);//se determina el largo de la frase 
	
	for(int i=0; i<largo; i++){       // se inicia un for para determinar cuantas veces se repite el caracter
		if ((frase[i])== caracter){   //se busca el caracter en la frase 
		contar++;	                  //se le agrega +1 a la candidad de veces que se encuentra el caracter 
		}
	}
	printf("en su frase se repite %d veces su caracter\n", contar); //se imprime el valor que se queria saber 
	
	return 0;
}
