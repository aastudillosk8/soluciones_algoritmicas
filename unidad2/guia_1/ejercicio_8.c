#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main()
{

	int cantidad, similitudes; //Se definen las variables
	printf("Ingrese la cantidad de los arreglos que requiera utilizar\n"); //Se pide la cantidad de arreglos
	scanf("%d", &cantidad); //Se guardara la cantidad
	
	int LadoA[cantidad];//Se define el arreglo para A
	int LadoB[cantidad];//Se define el arreglo para B

	for(int i=0; i<cantidad; i++){//
		LadoA[i]=(rand()%100)+1;
		printf("%d ", LadoA[i]);//
	}
	
	printf("\n");

	for(int i=0; i<cantidad; i++){//
		LadoB[i]=(rand()%100)+1;
		printf("%d ", LadoB[i]);//
	}
	printf("\n");

	for(int i=0; i<cantidad; i++){ //

		for(int j=0; j<cantidad; j++){//

			if(LadoA[i]-LadoB[j]==0){//
				similitudes=similitudes+1;//
				LadoA[i]=0;//
				LadoB[j]=0;//
				j=cantidad;//
			}
		}
	}
	printf("Ambos arreglos tienen un total de %d diferencias, es decir", cantidad-similitudes); //
	
	if(cantidad-similitudes==0){ //
	
		printf(" son iguales"); //
	}
	
	else if(cantidad-similitudes<=2){//

		printf(" son casi iguales");
	}
	
	else if(cantidad-similitudes>=3 || cantidad-similitudes<=5){//

		printf(" se parecen ");
	}
	
	else if(cantidad-similitudes>5){ //
		
		printf(" no son nada iguales");
	}
	return 0;
}

