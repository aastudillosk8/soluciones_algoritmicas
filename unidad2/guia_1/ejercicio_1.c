#include <stdio.h>
#include <stdlib.h>
#include <time.h>

//~ Escriba un programa que tome un arreglo de números y devuelva la suma acumulada, es decir, un
//~ nuevo arreglo donde el primer elemento es el mismo, el segundo elemento es la suma del primero
//~ con el segundo, el tercer elemento es la suma del resultado anterior con el siguiente elemento y ası́
//~ sucesivamente. Por ejemplo, la suma acumulada de [1,2,3] es [1, 3, 6].

int main(int argc, char **argv)
{	
	int largo;

	printf(" ingresa tamaño del arreglo\n");
	scanf("%d", &largo);
	int arr[largo];
	int arr_2[largo];
	int suma, i;
	
	srand(time(0));
	
	for(i=0; i<largo; i++){
		
		arr[i] = (rand()%100);
	}
	
	
	for(i=0; i<largo; i++){
		printf("%d ", arr[i]);
	}
	
	for (i=0; i<largo; i++){
		
		suma = arr[i] + suma;
		arr_2[i] = suma;
		
		printf("la suma es %d \n", arr_2[i]);
	}
	
	return 0;
}

