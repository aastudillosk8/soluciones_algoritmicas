#include <stdio.h>

int main(int argc, char **argv)
{
	int largo, i;
	printf("ingrese el largo del arreglo");
	scanf("%d", &largo);
	
	int arr_A[largo], arr_B[largo], arr_C[largo];
	
	printf("ingrese valores de A\n");
	for(i=0; i<largo; i++){
		scanf("%d", &arr_A[i]);
	}
	printf("ingrese valores de B\n");
	for(i=0; i<largo; i++){
		scanf("%d", &arr_B[i]);
	}
	
	printf("\n A: ");
	for(i=0; i<largo; i++){
		printf("%d ", arr_A[i]);
	}
	printf("\n B: ");
	for(i=0; i<largo; i++){
		printf("%d ", arr_B[i]);
	}
	
	for(i=0; i<largo; i++){
		if(arr_A[i] == arr_B[i]){
			arr_C[i] = arr_A[i];
		}
		if(arr_A[i] < arr_B[i]){
			arr_C[i] = 2 * (arr_B[i] - arr_A[i]);
		}
		if(arr_A[i] > arr_B[i]){
			arr_C[i] = arr_B[i];
		}
	}
	printf("\n C: ");
	for(i=0; i<largo; i++){
		printf("%d ", arr_C[i]);
	}
	
	return 0;
}

